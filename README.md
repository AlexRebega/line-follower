# README #

### Orion - Full line follower robot project ###

* Linefollower robot with PID control
* Second prototip
* uC: AVR Atmega 32u4

### Robot is capable of ###

* Follows a black line on a white surfac
* 'Line sensors' can be adjusted for different surfaces
* Star/Stop actions with universal remote controll
* Obstacle avoiding (read more below !)
* Moves pretty fast !

### Repo contains ###

* Eagle CAD Files: schematic(.sch) and board layout (.brd)
* Atmel Studio project
* Main program: GccApplication1/GccApplication1.c
* UART lib and Bluetooth lib


### Program ###

I implemented two PID algorithms. First is quit simple and works fine - just call pid_compute(int position) function. Second is fancier but I could not see a better performance. Second one was inspierd by Robot`s Zero algorithm wich you can see in zero_alg.c . It uses get_P_err(), get_D_err(), get_I_err(), and Compute_command().

Orion was designed to participate in Linefollower Enchanted contest, meaning it has to avoid a brick and go over a balancing bridge. This was improvised mainly on the spot, during the contest - ocoleste() functions counts how many obstacles have seen and goes around or over. Because I imporvised a lot on the spot, the main function got all messy (oh, and sorry for the comments writen in both english and romanian) but I was too lazy too 'clean it up'.

Robot has a brushless motor in center control by a ESC to maximaze adhezion. 
Bluetooth communication was used through UART protocol, for debug.
Driver motors are commanded using "Lock-antiphase" (so 50% duty cycle required to stop motors) 

If you want to build this robot, be aware of how you set your fusebits and what frequency you use for readings/commands.

*Mail me at rebega.alex@gmail.com if you need more informations !