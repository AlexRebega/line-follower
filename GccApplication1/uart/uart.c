
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include "uart.h"


/*************************************************************************
Function: uart_init()
Purpose:  initialize UART and set baudrate
Input:    baudrate using macro UART_BAUD_SELECT()
Returns:  none
**************************************************************************/
void uart_init(long int ubrr)
{
	cli();	
  /*Set baud rate */
  //UCSRA = (1 << U2X);
  UBRRH = (unsigned char)(ubrr>>8);
  UBRRL = (unsigned char)ubrr;
  /* Enable receiver and transmitter */
  UCSRB = (1<<RXEN)|(1<<TXEN);
  /* Set frame format: 8data, 2stop bit */
/*  UCSRC = (1<<USBS)|(3<<UCSZ0);*/
  UCSRC = (1<<URSEL)|(1<<USBS)|(1<<UCSZ0)|(1<<UCSZ1);
}/* uart_init */

/*************************************************************************
Function: uart_getc()
Purpose:  return byte from ringbuffer  
Returns:  lower byte:  received byte from ringbuffer
          higher byte: last receive error
**************************************************************************/


/*************************************************************************
Function: uart_putc()
Purpose:  write byte to ringbuffer for transmitting via UART
Input:    byte to be transmitted
Returns:  none          
**************************************************************************/
void uart_putc(unsigned char data) 
{
   /* Wait for empty transmit buffer */
   while ( !( UCSRA & (1<<UDRE)) );
   /* Put data into buffer, sends the data */
   UDR = data;

}/* uart_putc */
void USART_Transmit( unsigned char data )
{
	/* Wait for empty transmit buffer */
	while ( !( UCSRA & (1<<UDRE)) )
	;
	/* Put data into buffer, sends the data */
	UDR = data;
}


unsigned char uart_rcv(unsigned char *x, unsigned char size)
{
	unsigned char i = 0;

	if (size == 0) return 0;            // return 0 if no space

	while (i < size - 1) {              // check space is available (including additional null char at end)
		unsigned char c;
		while ( !(UCSRA & (1<<RXC)) );  // wait for another char - WARNING this will wait forever if nothing is received
		c = UDR;
		if (c == '\r' || c == '\n') break;           // break on NULL character
		x[i] = c;                       // write into the supplied buffer
		i++;
	}
	x[i] = 0;                           // ensure string is null terminated

	return i + 1;                       // return number of characters written
}

/*************************************************************************
Function: uart_puts()
Purpose:  transmit string to UART
Input:    string to be transmitted
Returns:  none          
**************************************************************************/
void uart_puts(const char *StringPtr )
{
   while(*StringPtr != 0x00)
   {
	   uart_putc(*StringPtr);
	   StringPtr++;
   }

}/* uart_puts */



