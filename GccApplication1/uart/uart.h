#ifndef UART_H
#define UART_H


void uart_init(long int ubrr);
void uart_putc(unsigned char data);
unsigned char uart_rcv(unsigned char *x, unsigned char size);
void uart_puts(const char *StringPtr );
void USART_Transmit( unsigned char data );

#endif