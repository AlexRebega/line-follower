/* Algoritm final*/

/*
 * GccApplication1.c

 *  Author: Alex
 ---------------------------------------------------------------------
 drv 8833 pins : ( we use lock-antiphase mode )
 PA1 - nSLEEP (HIGH to enable drv )
 PD4 - input for LEFT MOTOR
 PD5 - input for RIGHT MOTOR
 ---------------------------------------------------------------------
 PORTC used for line sensors 
 PC0 - rightmost sensor
 PC7 - leftmost sensor
 ---------------------------------------------------------------------

*/
#include ".\uart\uart.h"
#include ".\BL\bluetooth.h"
#include <avr/io.h>
#define F_CPU 8000000UL
#include <util/delay.h>
#include <avr/interrupt.h>
#include <stdlib.h>


#define MOTORS_ON PORTA |= (1<< PA1 );
#define MOTORS_OFF PORTA &= ~(1<< PA1 );
#define MOTORS_DDR DDRD
#define STOP_M 127
#define RIGHT_MOTOR OCR1A
#define LEFT_MOTOR OCR1B

#define ESC_DEFAULT 186

#define SET_POINT 7


#define SW_DDR DDRB
#define START_bit PB1
#define KILL_bit PB2
#define SW_PIN PINB

#define SENZOR_DIST PA3
#define Sensor_DDR	DDRC
#define Sensor_PORT PORTC
#define Sensor_PIN	PINC
#define Sensor1 PC0  //from right to left
#define Sensor2 PC1
#define Sensor3 PC2
#define Sensor4 PC3
#define Sensor5 PC4
#define Sensor6 PC5
#define Sensor7 PC6
#define Sensor8 PC7

#define DEBUG_MODE 0

int Kd, Kp;
float Ki;
int last_val;	//used by old algorithm
volatile int speed = 190;
char str[10] ; // used for debug
int x,y; 
int MAX_SPEED ;


void motor_init(void)
{
		DDRA |= (1<< PA1 );
		//PORTA |= (1<< PA1 );
		// enable/disable drv (nSLEEP high)
		
		MOTORS_DDR |= (1<< PD4) | (1<< PD5); // set motors as output
 
		// set WGM10 & 12 - FAST PWM, 8 bit ; 
		// CS10 - no prescaling
		//COM1A1/COM1B1 si COM1A0/COM1B0 (non inverting)
		TCCR1A |= (1<< WGM10)|(1<< COM1A0)|(1<< COM1A1)|(1<< COM1B0)|(1<< COM1B1);
		TCCR1B |= (1<< WGM12)|(1<< CS10);
		
		RIGHT_MOTOR = 127; // PWMs duty cycle is 50% 
		LEFT_MOTOR = 127; // both motors are stopped
		
}

void remote_SW_init(void)
{
	SW_DDR &= !(1<<START_bit);
	SW_DDR &= !(1<<KILL_bit);

}


void sensors_init(void)
{
	Sensor_DDR = 0x00;	// set sensors pins as input
	Sensor_PORT = 0xFF;	// pull-up for line sensors enabled
}

void interrupt_timer_init()
{
	// configure timer2 for 1ms/10ms/20ms period
	//TODO: activate OC2 and look with a oscilloscope at pin PD7(OC2)(to verify period)
	OCR2 = 254;	//OCR2 = 10 =>1.9ms ;  OCR2 = 25 =>4ms ; OCR2 = 20 =>3.6ms ;  OCR2 = 50 =>8.77ms
	/*set WGM21 for CTC mode,set CS0:2 for 1024 prescaler => 11.7KHz */
	TCCR2 |= (1<<WGM21)|(1<<CS22)|(1<<CS21)|(1<<CS20);
	// enable interrupt on output compare math
	TIMSK |= (1<<OCIE2); 
	sei();
}

void ESC_init(void)
{
	DDRB |= (1 << PB3);// Fast PWM, Non inverting mode(clear OC0 on comp match)
	TCCR0 |=(1<<WGM00)|(1<<WGM01)|(1<<COM01)|(1<<CS02);// clk/256(from prescaler)
	OCR0 = 50;
	_delay_ms(200);		// f= 12MHZ/ (256*256) = 183 Hz
}


void ESC_SET (uint8_t dc)
{
	OCR0 = dc;
}


inline void set_motors(int left, int right)
{
	RIGHT_MOTOR = right;	// 0 - 127 - motors are spining backwards, 127 - 255 - forward (255 max speed)
	LEFT_MOTOR = left;	// TODO - scrie o functie de mapare a comenzii
}


// old algorithm


int read_sensors() {
	long sum=0, average=0;
	unsigned char a;

	uint8_t i;

	uint8_t pins = PINC;

	for(i = 0; i < 8; i++) {
		a = pins & 1;
		sum += a;
		average += a * i * 2;
		pins >>= 1;
	}
	if(sum == 0)
		return last_val;
	else
		last_val =average/sum;
	return last_val;
}


int PID_compute(int pozition)
{
	static int proportional, derivative,integral, last_proportional;
	int error_value;
	//if((pozition <= 1) && (pozition >= -1))
		//return 0;
	
	proportional = pozition - SET_POINT; // good name ? is this not the error ?
	//integral = integral + proportional; // For start we will use just PD;
	derivative = proportional - last_proportional;
	last_proportional = proportional;
	integral+=proportional;
	error_value = proportional *Kp + integral*Ki + derivative*Kd;// good name ? is this not the command ?
	
	return error_value;
}


void command_motors(int error_value)
{
	// if error < 0 turn right
	// else turn left
	if(error_value < 0)
	{
		if(error_value < -MAX_SPEED) // should I compare with MAX_SPEED or 255 ?
				error_value = -MAX_SPEED;
		set_motors(MAX_SPEED, MAX_SPEED + error_value); // is this good ? double-check the values
	}
	else
	{
		if(error_value > MAX_SPEED)
			error_value = MAX_SPEED;
		set_motors(MAX_SPEED - error_value, MAX_SPEED);
	}

} 



//new algorithm
int get_P_err(void)
{
	int errorp=0;
	static int last_errorp=0;
	int count_sensors=0;

	if((bit_is_set(Sensor_PIN,Sensor4) != 0) && (bit_is_set(Sensor_PIN,Sensor5) != 0))
	{
		errorp=0;
		return(0);
	 }

	if(bit_is_set(Sensor_PIN,Sensor1) != 0) 
	{
		errorp = errorp - 7;
		count_sensors ++;
	}
	if(bit_is_set(Sensor_PIN,Sensor2) != 0) 
	{
		errorp = errorp - 5;
		count_sensors ++;
	}
	if(bit_is_set(Sensor_PIN,Sensor3) != 0) 
	{
		errorp = errorp - 3;
		count_sensors ++;
	}
	if(bit_is_set(Sensor_PIN,Sensor4) != 0) 
	{
		errorp = errorp - 1;
		count_sensors ++;
	}
	if(bit_is_set(Sensor_PIN,Sensor5) != 0) 
	{
		errorp = errorp + 0x01;
		count_sensors ++;
	}
	if(bit_is_set(Sensor_PIN,Sensor6) != 0) 
	{
		errorp = errorp + 0x03;
		count_sensors ++;
	}
	if(bit_is_set(Sensor_PIN,Sensor7) != 0)
	{
		errorp = errorp + 0x05;
		count_sensors ++;
	}
	if(bit_is_set(Sensor_PIN,Sensor8) != 0)
	{
		errorp = errorp + 0x07;
		count_sensors ++;
	}

   
	if(count_sensors != 0)
	{
		errorp = errorp / count_sensors;
		last_errorp = errorp;
		return(Kp * (int)errorp);
	}
	else
	{
		if(last_errorp < 0)
			errorp = -0x09;
		else
			errorp = 0x09;

		last_errorp = errorp;
		return((int)errorp * Kp);
	}
}



int get_D_error(void)
{
	int error = 0;
	static int error_old = 0;
	static int errord=0;
	static int errord_old = 0;
	static int tic = 1;  
	static int tic_old = 1; 

	int difference = 0;

	if((bit_is_set(Sensor_PIN,Sensor4) != 0) && (bit_is_set(Sensor_PIN,Sensor5) != 0))
		error = 0;

	else if(bit_is_set(Sensor_PIN,Sensor1) != 0) 
		error = -7;

	else if(bit_is_set(Sensor_PIN,Sensor2) != 0) 
		error = -5;

	else if(bit_is_set(Sensor_PIN,Sensor3) != 0) 
		error = -3;

	else if(bit_is_set(Sensor_PIN,Sensor4) != 0) 
		error = -1;

	else if(bit_is_set(Sensor_PIN,Sensor5) != 0) 
		error = 1;

	else if(bit_is_set(Sensor_PIN,Sensor6) != 0) 
		error = 3;

	else if(bit_is_set(Sensor_PIN,Sensor7) != 0) 
		error = 5;

	else if(bit_is_set(Sensor_PIN,Sensor8) != 0) 
		error = 7;

	else
		{
			if (error_old < 0)
				error = -9;
			else if(error_old > 0)  // init >
				error = 9;
		}

	if (error == error_old)
	{
		tic = tic + 1;
		if(tic > 18000)		// old val - 30000
			tic = 18000;
		if(tic > tic_old)
			errord = errord_old/tic;
			//errord = errord_old;
	}
	else
	{
		tic++;
		difference = error - error_old;
		errord = Kd*(difference)/tic; //medium error
		//errord = Kd*(difference);
		errord_old = errord;
		tic_old=tic;
		tic=0;
	}

	error_old = error;
	return(errord);
}

void compute_command(void)
{
	int errort=0;
	int proportional = get_P_err();
	int derivative = get_D_error();

	errort = proportional + derivative;


	if(errort > speed)
		errort = speed;
	else if(errort < - speed)
		errort = - speed;
	
 	if(errort>0) // turn left
    	{set_motors(speed - errort, speed);
        //debug
		if(DEBUG_MODE){
        uart_puts("Curba stanga: "); 
        uart_puts(itoa((speed - errort),str,10));
        uart_puts("  ");
        uart_puts(itoa(speed,str,10));
        uart_puts("\n\r");
        _delay_ms(300);
		}
      }        
		
	else if(errort<0) // turn right
		{set_motors(speed,speed + errort);
		if(DEBUG_MODE){
		//debug
        uart_puts("Curba dreapta: "); 
        uart_puts(itoa(speed,str,10));
        uart_puts("  ");
        uart_puts(itoa((speed + errort),str,10));
        uart_puts("\n\r");
        _delay_ms(300);
		}
      }

	else
		set_motors(speed,speed);        /* uart_puts("FullSpeed\n\r"); */ 
    
    
	//debug 
	     if(DEBUG_MODE){ 
	uart_puts("P: ");
    uart_puts(itoa(proportional,str,10));
	uart_puts("		D: ");
    uart_puts(itoa(derivative,str,10));  
	uart_puts("    errort: ");
	uart_puts(itoa(errort,str,10));
	uart_puts("\n\r");
		 }
  
  
}

void ocoleste(void)
 {   
	static int k = 0;
				k++;
				/*
				 if(k==0)
				 {

				
				 _delay_ms(500);			
				 set_motors(127,127);
				 _delay_ms(200);	
				 }
				 k++;
				 if(k == 1)
				 _delay_ms(1000);
				 
				 if(k==1)
				 {
				 set_motors(200,200);
				 _delay_ms(700);
				 }
				 */
				 if(k==3) _delay_ms(100);
				 if(k > 3)
				  {
				 
				  set_motors(210,30); // dreapta
				  _delay_ms(120);
				  
				 set_motors(200,200); //inainte
				 _delay_ms(130);
				 
				 //MOTORS_OFF
				 //_delay_ms(1000);
				 set_motors(30,210); //stanga
				 _delay_ms(80);
				 
				 set_motors(200,200); // inainte
				 _delay_ms(190);
				 
				 set_motors(20,210); //stanga
				 _delay_ms(140);
				 
				 //set_motors(190,190);
				 while((PINC & (1<<PC0))==0)
				 {  
					 set_motors(190,190);
				 }
				 set_motors(210,30);
				 _delay_ms(10);
				 
	}
				 

			 }

int timp = 0;
int main(void)
{	


	//uart_init(77);	// set bluetooth baud rate at 9600 for clk frq 12Mz
					// config. UART as Asynchronous Normal, no parity, 2 stop bits, 8-bit format;

	//remote_SW_init();
	motor_init();
	sensors_init();

//	if(DEBUG_MODE)
	MOTORS_ON
	set_motors(STOP_M,STOP_M);
	
	last_val = SET_POINT;	// old alg.
	Kp = 25;
	Kd = 90;
	MAX_SPEED = 215;
	Ki=0.08;
	//interrupt_timer_init();
	// bun 26,90,215
	//25,95,2150
    //22 90 210
	//24 80? 210
	
	/*
    x=1;
	_delay_ms(100);
	
	while (PINB & (1<<PB1))
	{;
	}
	*/


	
  	
	_delay_ms(2000);
	
    
    while(1)
    {
		// ocolire obstacole	
	if(PINA & (1<<PA3))
		     ocoleste();
			 
			 
		command_motors(PID_compute(read_sensors()));
		_delay_ms(13);
		/*	 
		 _delay_ms(20);
 		compute_command();
 		if(bit_is_clear(SW_PIN,2))
	 			{MOTORS_OFF}
	    */	
	}
}

ISR(TIMER2_COMP_vect)
{
	timp ++;
	TIFR |= (1<<OCF2); // set this bit to clear Output compare flag of TIMER2
	
	
}
