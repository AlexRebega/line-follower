/* Zero Algorithm */

/*
 * GccApplication1.c

 *  Author: Alex
 ---------------------------------------------------------------------
 drv 8833 pins : ( we use lock-antiphase mode )
 PA1 - nSLEEP (HIGH to enable drv )
 PD4 - input for LEFT MOTOR
 PD5 - input for RIGHT MOTOR
 ---------------------------------------------------------------------
 PORTC used for line sensors 
 PC0 - rightmost sensor
 PC7 - leftmost sensor
 ---------------------------------------------------------------------

*/
#include ".\uart\uart.h"
#include ".\BL\bluetooth.h"
#include <avr/io.h>
#define F_CPU 12000000UL
#include <util/delay.h>
#include <avr/interrupt.h>
#include <stdlib.h>

#define MOTORS_ON PORTA |= (1<< PA1 );
#define MOTORS_OFF PORTA &= ~(1<< PA1 );
#define MOTORS_DDR DDRD
#define ESC_DEFAULT 186
#define MAX_SPEED 255
//#define SET_POINT 7
#define RIGHT_MOTOR OCR1A
#define LEFT_MOTOR OCR1B
#define Sensor_DDR	DDRC
#define Sensor_PORT PORTC
#define Sensor_PIN	PINC
#define Sensor1 PC0  //from right to left
#define Sensor2 PC1
#define Sensor3 PC2
#define Sensor4 PC3
#define Sensor5 PC4
#define Sensor6 PC5
#define Sensor7 PC6
#define Sensor8 PC7
int Ki, Kd, Kp;
int last_val;
volatile int speed = 190;


void motor_init(void)
{
		DDRA |= (1<< PA1 );
		//PORTA |= (1<< PA1 );
		// enable/disable drv (nSLEEP high)
		
		MOTORS_DDR |= (1<< PD4) | (1<< PD5); // set motors as output
 
		// set WGM10 & 12 - FAST PWM, 8 bit ; 
		// CS10 - no prescaling
		//COM1A1/COM1B1 si COM1A0/COM1B0 (non inverting)
		TCCR1A |= (1<< WGM10)|(1<< COM1A0)|(1<< COM1A1)|(1<< COM1B0)|(1<< COM1B1);
		TCCR1B |= (1<< WGM12)|(1<< CS10);
		
		RIGHT_MOTOR = 127; // PWMs duty cycle is 50% 
		LEFT_MOTOR = 127; // both motors are stopped
		
}


void sensors_init(void)
{
	Sensor_DDR = 0x00;	// set sensors pins as input
	Sensor_PORT = 0xFF;	// pull-up for line sensors enabled
}


void ESC_init(void)
{
	DDRB |= (1 << PB3);
	TCCR0 |=(1<<WGM00)|(1<<WGM01)|(1<<COM01)|(1<<CS00)|(1<<CS01);
	OCR0 = ESC_DEFAULT;
	_delay_ms(200);
}


void ESC_SET (uint8_t dc)
{
	OCR0 = dc;
}


inline void set_motors(int left, int right)
{
	RIGHT_MOTOR = right-2;	// 0 - 127 - motors are spining backwards, 127 - 255 - forward (255 max speed)
	LEFT_MOTOR = left;	// TODO - scrie o functie de mapare a comenzii
}

/*
// old algorithm


int read_sensors() {
	long sum=0, average=0;
	unsigned char a;

	uint8_t i;

	uint8_t pins = PINC;

	for(i = 0; i < 8; i++) {
		a = pins & 1;
		sum += a;
		average += a * i * 2;
		pins >>= 1;
	}
	if(sum == 0)
		return last_val;
	else
		last_val =average/sum;
	return last_val;
}


int PID_compute(int pozition)
{
	static int proportional, integral, derivative, last_proportional;
	int error_value;
	
	proportional = pozition - SET_POINT; // good name ? is this not the error ?
	//integral = integral + proportional; // For start we will use just PD;
	derivative = proportional - last_proportional;
	last_proportional = proportional;
	error_value = proportional *Kp + integral*Ki + derivative*Kd;// good name ? is this not the command ?
	
	return error_value;
}


void command_motors(int error_value)
{
	// if error < 0 turn right
	// else turn left
	if(error_value < 0)
	{
		if(error_value < -MAX_SPEED) // should I compare with MAX_SPEED or 255 ?
				error_value = -MAX_SPEED;
		set_motors(MAX_SPEED, MAX_SPEED + error_value); // is this good ? double-check the values
	}
	else
	{
		if(error_value > MAX_SPEED)
			error_value = MAX_SPEED;
		set_motors(MAX_SPEED - error_value, MAX_SPEED);
	}
	
} 
*/


//new algorithm
int get_P_err(void)
{
	char errorp=0;
	static char last_errorp=0;
	char count_sensors=0;

	if(((Sensor_PIN & Sensor5) != 0) && ((Sensor_PIN & Sensor4) != 0))
	{
		errorp=0;
		return(0);
	 }

	if((Sensor_PIN & Sensor1) != 0) 
	{
		errorp = errorp - 0x07;
		count_sensors ++;
	}
	if((Sensor_PIN & Sensor2) != 0) 
	{
		errorp = errorp - 0x05;
		count_sensors ++;
	}
	if((Sensor_PIN & Sensor3) != 0) 
	{
		errorp = errorp - 0x03;
		count_sensors ++;
	}
	if((Sensor_PIN & Sensor4) != 0) 
	{
		errorp = errorp - 0x01;
		count_sensors ++;
	}
	if((Sensor_PIN & Sensor5) != 0) 
	{
		errorp = errorp + 0x01;
		count_sensors ++;
	}
	if((Sensor_PIN & Sensor6) != 0) 
	{
		errorp = errorp + 0x03;
		count_sensors ++;
	}
	if((Sensor_PIN & Sensor7) != 0)
	{
		errorp = errorp + 0x05;
		count_sensors ++;
	}
	if((Sensor_PIN & Sensor8) != 0)
	{
		errorp = errorp + 0x07;
		count_sensors ++;
	}

	if(count_sensors != 0)
	{
		errorp = errorp / count_sensors;
		last_errorp = errorp;
		return(Kp * (int)errorp);
	}
	else
	{
		if(last_errorp < 0)
			errorp = -0x09;
		else
			errorp = 0x09;

		last_errorp = errorp;
		return((int)errorp * Kp);
	}
}



int get_D_error(void)
{
	int error = 0;
	static int error_old = 0;
	static int errord=0;
	static int errord_old = 0;
	static int tic = 1;  
	static int tic_old = 1; 

	int difference = 0;

	if(((Sensor_PIN & Sensor5) != 0) && ((Sensor_PIN & Sensor4) != 0))
		error = 0;

	else if((Sensor_PIN & Sensor1) != 0) 
		error = -7;

	else if((Sensor_PIN & Sensor2) != 0) 
		error = -5;

	else if((Sensor_PIN & Sensor3) != 0) 
		error = -3;

	else if((Sensor_PIN & Sensor4) != 0) 
		error = -1;

	else if((Sensor_PIN & Sensor5) != 0) 
		error = 1;

	else if((Sensor_PIN & Sensor6) != 0) 
		error = 3;

	else if((Sensor_PIN & Sensor7) != 0) 
		error = 5;

	else if((Sensor_PIN & Sensor8) != 0) 
		error = 7;

	else
		{
			if (error_old < 0)
				error = -9;
			else if(error_old > 0)
				error = 9;
		}

	if (error == error_old)
	{
		tic = tic + 1;
		if(tic > 30000)		// good value ?  do some more experiments
			tic = 30000;
		if(tic > tic_old)
			errord = errord_old/tic;
//		if(tic > tic_old)		// ??
//			errord = (errord_old*tic_old)/tic;

	}
	else
	{
		tic++;
		difference = error - error_old;
		errord = Kd*(difference)/tic; //error medio
		errord_old = errord;
		tic_old=tic;
		tic=0;
	}

	error_old = error;
	return(errord);
}

void do_stuff(void)
{
	int errort=0;
	int proportional = get_P_err();
	int derivative = get_D_error();


	errort = proportional + derivative;


	if(errort > velocidad)
		errort = velocidad;
	else if(errort < - velocidad)
		errort = - velocidad;
	
	if(errort>0)
	{
    	M1_forward(velocidad - errort);     //Motor derecho.
        M2_forward(velocidad);              //Motor izquierdo.
	    PORTB |= (1<<LEDV);
		PORTB &= ~(1<<LEDR);
	}
	else if(errort<0)
	{
    	M1_forward(velocidad);              //Motor derecho.  
        M2_forward(velocidad + errort);     //Motor izquierdo. 
	    PORTB |= (1<<LEDR);
		PORTB &= ~(1<<LEDV);
	}

	else
	{
    	M2_forward(velocidad);       
        M1_forward(velocidad);
		PORTB &= ~(1<<LEDR);
	    PORTB &= ~ (1<<LEDV);
	}

}


int main(void)
{	
	
	uart_init(77);	// set bluetooth baud rate at 9600 for clk frq 12Mz
	//sei();
	motor_init();
	sensors_init();
		
	MOTORS_ON
	set_motors(127,127);

	//ESC_init();	
	_delay_ms(400);
	//ESC_SET(257);
	
	last_val = SET_POINT;
	Kp = 17;
	Ki = 0;
	Kd = 41;


    while(1)
    {	

		//command_motors(PID_compute(read_sensors(last_val)));
		_delay_ms(30);

	}
		
}